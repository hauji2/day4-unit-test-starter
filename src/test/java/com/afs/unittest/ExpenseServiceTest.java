package com.afs.unittest;

import com.afs.unittest.exception.ProjectExpiredException;
import com.afs.unittest.exception.UnexpectedProjectTypeException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ExpenseServiceTest {

    @Test
    void should_return_internal_expense_type_when_getExpenseCodeByProject_given_internal_project() {
        // given
        final var internalProject = new Project(ProjectType.INTERNAL, "Project Internal");
        final var projectClient = new ProjectClient();
        final var expenseService = new ExpenseService(projectClient);
        // when
        final var expenseCodeByProject = expenseService.getExpenseCodeByProject(internalProject);
        // then
        Assertions.assertEquals(ExpenseType.INTERNAL_PROJECT_EXPENSE, expenseCodeByProject);
    }

    @Test
    void should_return_expense_type_A_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_A() {
        // given
        Project project = new Project(ProjectType.EXTERNAL, "Project A");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        ExpenseType expect = ExpenseType.EXPENSE_TYPE_A;
        // when
        ExpenseType actual = expenseService.getExpenseCodeByProject(project);
        // then
        Assertions.assertEquals(expect, actual);

    }

    @Test
    void should_return_expense_type_B_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_B() {
        // given
        Project project = new Project(ProjectType.EXTERNAL, "Project B");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        ExpenseType expect = ExpenseType.EXPENSE_TYPE_B;

        // when
        ExpenseType actual = expenseService.getExpenseCodeByProject(project);
        // then
        Assertions.assertEquals(expect, actual);
    }

    @Test
    void should_return_other_expense_type_when_getExpenseCodeByProject_given_project_is_external_and_has_other_name() {
        // given
        Project project = new Project(ProjectType.EXTERNAL, "Project C");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        ExpenseType expect = ExpenseType.OTHER_EXPENSE;

        // when
        ExpenseType actual = expenseService.getExpenseCodeByProject(project);
        // then
        Assertions.assertEquals(expect, actual);
    }

    @Test
    void should_throw_unexpected_project_exception_when_getExpenseCodeByProject_given_project_is_invalid() {
        // given
        Project project = new Project(ProjectType.UNEXPECTED_PROJECT_TYPE, "Unexpected project type");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        assertThrows(UnexpectedProjectTypeException.class, () -> {
            expenseService.getExpenseCodeByProject(project);
        });
    }

    @Test
    void should_return_submit_state_when_submit_expense_given_project_do_not_expired() {
        // given
        Project project = new Project(ProjectType.INTERNAL, "Project B");
//        ProjectClient projectClient = mock(ProjectClient.class);
        FakeProjectClient projectClient = new FakeProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        ExpenseState expect = ExpenseState.SUBMIT;

//        doReturn(false).when(projectClient).isExpired(any(Project.class));

        // when
        ExpenseState actual = expenseService.submitExpense(project);

        // then
        Assertions.assertEquals(expect, actual);
    }

    @Test
    void should_throw_expired_exception_when_submit_expense_given_project_has_expired() {
        // given
        Project project = new Project(ProjectType.INTERNAL, "Project A");
//        ProjectClient projectClient = mock(ProjectClient.class);
        FakeProjectClient projectClient = new FakeProjectClient();
        projectClient.setResult(true);

        ExpenseService expenseService = new ExpenseService(projectClient);

//        when(projectClient.isExpired(project)).thenReturn(true);
        // when
        assertThrows(ProjectExpiredException.class, () -> {
            expenseService.submitExpense(project);
        });
    }
}
