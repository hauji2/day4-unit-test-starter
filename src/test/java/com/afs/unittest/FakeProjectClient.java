package com.afs.unittest;

public class FakeProjectClient extends ProjectClient {
    private boolean result;

    @Override
    public boolean isExpired(Project project) {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
